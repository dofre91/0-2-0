﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiKaizala.BusinessLayer
{
    class GlobalPatameters
    {
            /// <summary>
            /// access_token which was created
            /// </summary>
            [JsonProperty("access_token")]
            public string access_token;

        /// <summary>
        /// access_token which was created
        /// </summary>
        /// 
            [JsonProperty(PropertyName = "kaizalaGroupId")]
            public string kaizalaGroupId;

    }
}
