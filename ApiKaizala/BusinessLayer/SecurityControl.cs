﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static ApiKaizala.BusinessLayer.GlobalPatameters;

namespace ApiKaizala.BusinessLayer
{
    class SecurityControl
    {

        //Method to send messages from SQL - KAIZALA
        public String GetToken()
        {
            string Token = "";
            var client = new RestClient("https://login.microsoftonline.com/bdef9893-87ef-40e4-97a4-c7d985698696/oauth2/token");//production
            //var client = new RestClient("https://login.microsoftonline.com/78abe219-0685-4a10-b261-d7318bdae74f/oauth2/token ");//QA
            var request = new RestRequest(Method.POST);

            //PRODuction 3 y 4
            request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
            request.AddParameter("grant_type", "client_credentials");
            request.AddParameter("client_id", "637c3fb4-c0f6-4ca9-8820-25742db6fab1");
            request.AddParameter("client_secret", "jvrrt~PS0LbSj-~~G73-Zej9PX9cvl8~2-");
            request.AddParameter("resource", "api://25a1c017-b8a6-4a39-9f15-3e6fbc776e63");

            //PRODuction 5 y 6
            //request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
            //request.AddParameter("grant_type", "client_credentials");
            //request.AddParameter("client_id", "bff1606d-e3c0-452c-a56c-5adec278dd7d");
            //request.AddParameter("client_secret", "@Pma/6SlRZoVW85J0dWU[xlCH@S.bHEt");
            //request.AddParameter("resource", "api://25a1c017-b8a6-4a39-9f15-3e6fbc776e63");

            ////QA
            //request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
            //request.AddParameter("grant_type", "client_credentials");
            //request.AddParameter("client_id", "4003bf3d-586c-42f1-af3b-ee029a6dd2d9");
            //request.AddParameter("client_secret", "phGwX/1_F8hDw3okuxEW/:9Lh?QbUfzb");
            //request.AddParameter("resource", "api://501efbab-d190-456a-9990-ff6cd8e8a71e");
            IRestResponse response = client.Execute(request);
            Token = response.Content;
            var loginWithPinAndApplicationIdResponse =
            Newtonsoft.Json.JsonConvert.DeserializeObject<GlobalPatameters>(response.Content);
            Token = loginWithPinAndApplicationIdResponse.access_token;
            return Token;
        }

    }
}
