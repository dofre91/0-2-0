﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using ApiKaizala.DataLayer;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;


namespace ApiKaizala.BusinessLayer
{
    class SendMessage
    {
        String accessToken;
        String NombreGrupo;
        String MensajeGrupo;
        String NomEquipo;
        String Producto;
        String LimitMin;
        String LimitMax;
        String ValueVar;
        String VarName;
        String VarNumber;
        String TpoNivel;
        String Inicio;
        int IdMessage;
        DataCollection Collection = new DataCollection();
        SecurityControl Security = new SecurityControl();
        DBConnection KaizalaCentral = new DBConnection();

        //Method to send messages from SQL - KAIZALA
        public void SenMessageToGroup()
        {
            KaizalaCentral.conectar();
            DataTable DatosKaizala = KaizalaCentral.ConsultarGenerico(Collection.mX_Kzl_GetVarAlerts);
            accessToken = "Bearer " + Security.GetToken();

            foreach (DataRow row in DatosKaizala.Rows)
            {
                try
                {
                   // NombreGrupo = row["Grupo"].ToString();
                    NombreGrupo = row["GroupList"].ToString(); ;
                    IdMessage = Convert.ToInt32(row["Id_AlertData"].ToString());
                    NomEquipo = row["MfPlant"].ToString();
                    Producto = row["Producto"].ToString();
                    LimitMin = row["LimitMin"].ToString();
                    LimitMax = row["LimitMax"].ToString();
                    ValueVar = row["ValueVar"].ToString();
                    VarName = row["VarName"].ToString();
                    VarNumber = row["VarNumber"].ToString();
                    MensajeGrupo = "Nombre Producto: @@1\nMaquina: @@2\nNombre de Variable: @@3\nValor de Variable: @@4\nLimite Minimo: @@5\nLimite Maximo: @@6\n*** Variable Fuera de Rango ***\n**Mensaje de Prueba**";
                    if (MensajeGrupo.Equals(""))
                    {
                        MensajeGrupo = "Sin demora registrada";
                    }
                    else
                    {
                        MensajeGrupo = MensajeGrupo.Replace("@@1", Producto);
                        MensajeGrupo = MensajeGrupo.Replace("@@2", NomEquipo);
                        MensajeGrupo = MensajeGrupo.Replace("@@3", VarName);
                        MensajeGrupo = MensajeGrupo.Replace("@@4", ValueVar);
                        MensajeGrupo = MensajeGrupo.Replace("@@5", LimitMin);
                        MensajeGrupo = MensajeGrupo.Replace("@@6", LimitMax);

                    }

                    String IdGrupo = GetIdGroupByName(NombreGrupo, accessToken);
                    String MessageAndon = " {\"message\": \"" + MensajeGrupo + "\", \n \"groupID\":\"" + IdGrupo + "\"}";
                    var client = new RestClient("https://pro-nemakkaizalaadminportal.azurewebsites.net/api/SendMessageKaizala");//PROductivo
                    //var client = new RestClient("https://kaizlaadminportal.azurewebsites.net/api/SendMessageKaizala");//PROductivo
                    var request = new RestRequest(Method.POST);
                    request.AddHeader("Authorization", accessToken);
                    request.AddParameter("application/json", MessageAndon, ParameterType.RequestBody);
                    IRestResponse response = client.Execute(request);
                    KaizalaCentral.conectar();
                    KaizalaCentral.CambiarStatusKaizala(Collection.mX_Kzl_SetVarAlerts, IdMessage);
                }
                catch (Exception e)
                {

                    Console.WriteLine(e.Message);

                }
            //}
            KaizalaCentral.desconectar();
            }
        }


        //Method to send messages with Attachment from SQL - KAIZALA
        public void SendMessageWithAttachmentToGroup(String NameOfGroup)
        {
            try
            {
                KaizalaCentral.conectar();
                accessToken = "Bearer " + Security.GetToken();
                String IdGrupo = GetIdGroupByName(NameOfGroup, accessToken);
                //String IdGrupo = "5fb7a862-86fb-499d-8c42-02f19db0adf7";
                String URL = "https://pro-nemakkaizalaadminportal.azurewebsites.net/api/SendMessageWithAttachmentService?message=" + "Reporte OK Moldeo por horno - Planta 5" + "&groupid=" + IdGrupo + "";//prod
                //String URL = "https://kaizlaadminportal.azurewebsites.net/api/SendMessageWithAttachmentService?message=" + "PDF" + "&groupid=" + IdGrupo + ""; //QAS
                var client = new RestClient(URL);
                var request = new RestRequest(Method.POST);
                request.AddHeader("Authorization", accessToken);
                request.AddFile("file", @"D:\NORIS APPS\Kaizala\Files\Reporte OK Moldeo por horno - producto.pdf", "application/pdf");
               // request.AddFile("file", @"C:\Users\EX-JRETA\Downloads\Reporte OK Moldeo por horno - producto.jpg", "image/jpg");
                IRestResponse response = client.Execute(request);

                for (int i = 0; i < response.Headers.Count; i++)
                {
                    Type name = response.Headers.GetType();
                }

                    KaizalaCentral.desconectar();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public String GetIdGroupByName(String NameOfGroup, String TokenBarer)
        {
            String id = "";
            String ClientUrl = "https://pro-nemakkaizalaadminportal.azurewebsites.net/api/getGroupsByNameService?groupName=" + NameOfGroup + "";//Prod
           // String ClientUrl = "https://kaizlaadminportal.azurewebsites.net/api/getGroupsByNameService?groupName=" + NameOfGroup + "";//QAS
            var client = new RestClient(ClientUrl);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", TokenBarer);
            IRestResponse response = client.Execute(request);
            var loginWithPinAndApplicationIdResponse = JsonConvert.DeserializeObject<List<GlobalPatameters>>(response.Content);
            id = loginWithPinAndApplicationIdResponse[0].kaizalaGroupId.ToString();
            return id;
        }

        public void ExportToExcel()
        {

            var theURL = "http://mtysql0041/ReportServer?%2FNORIS%2FMTY_P5%2Fproduction%2FKPI_Report_BPTargets_Production&rs:Format=PDF";//PDF
            //var theURL = "http://mtysql0041/ReportServer/Pages/ReportViewer.aspx?%2FNORIS%2FMTY_P5%2Fproduction%2FKPI_Report_BPTargets_Production&rs:Format=IMAGE&rc:OutputFormat=jpg";//PNG
            WebClient Client = new WebClient();
            Client.UseDefaultCredentials = true;
            byte[] myDataBuffer = Client.DownloadData(theURL);

            String ruta = @"D:\NORIS APPS\Kaizala\Files\Reporte OK Moldeo por horno - producto.pdf";
            File.Delete(ruta);
            File.WriteAllBytes(@"D:\NORIS APPS\Kaizala\Files\Reporte OK Moldeo por horno - producto.pdf", myDataBuffer.ToArray());

        }

        public void SenMessagePrivateOrPublic(String NameOfGroup, String flag)
        {

            try
            {   
                String PrivateNumbers = "["+   "\"" +"+528343088168"+ "\"" + "]";
                accessToken = "Bearer " + Security.GetToken();
                MensajeGrupo = "Kaizala Private Demo";
                String IdGrupo = GetIdGroupByName(NameOfGroup, accessToken);
                String MessageAndon = " {\"message\": \"" + MensajeGrupo + "\", \n \"groupID\":\"" + IdGrupo + "\", \n \"allSubscribers\":\"" + flag + "\", " +
                    "\n \"subscribers\":" + PrivateNumbers + " }";
                var client = new RestClient("https://kaizlaadminportal.azurewebsites.net/api/SendMessageMemberKaizala");
                var request = new RestRequest(Method.POST);
                request.AddHeader("Authorization", accessToken);
                request.AddParameter("application/json", MessageAndon, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                KaizalaCentral.conectar();
                KaizalaCentral.CambiarStatusKaizala(Collection.nOR_SetTextMachineDelay, IdMessage);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
