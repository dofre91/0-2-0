﻿using ApiKaizala.SecurityLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Net.Mime.MediaTypeNames;

namespace ApiKaizala.DataLayer
{
    class DBConnection
    {
        static string variableConexionBD = ReturnServerConnection();
        public SqlConnection CadenaConexion = new SqlConnection(@"" + variableConexionBD + "");

        public void conectar()
        {
            try
            {
                CadenaConexion = new SqlConnection(@"" + variableConexionBD + "");
                CadenaConexion.Open();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        //metodo para llamar un procedimiento para consultar cualquier cosa solo procedimiento de consulta
        public DataTable ConsultarGenerico(String NameProcedure)
        {
            SqlCommand command = new SqlCommand(NameProcedure, CadenaConexion);
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.ExecuteNonQuery();
            DataTable dt = new DataTable();
            dt.Load(command.ExecuteReader());
            CadenaConexion.Close();
            return dt;
        }

        public static String ReturnServerConnection()
        {
            Cryptography Desc = new Cryptography();
            //String conn = Desc.descifrar("Vd5GM4lnlX+2FEZYfsfNcrl/Xr/LP62iJltR274z3bnBBObCx1+5qlbn3FejRpPH1c7EPJsvEbAd18MAC76cXFbn3FejRpPHZN1MvlBoAPy3CqOXR6+LWKUq4UyM0gCIX+5BMtuEGG1Jj+q2v+fl9g==");
            String conn2 = Desc.cifrar("Data Source=mtyaps0140.am.nemak.net;Initial Catalog=NORIS_Local_P04;User ID=KaizalaPrueba; Password=Desa@123");
            String conn = Desc.descifrar(conn2);
            return conn;
        }

        //metodo para llamar un procedimiento para consultar cualquier cosa solo procedimiento de consulta
        public void CambiarStatusKaizala(String NameProcedure ,int IdKaizalaMsj)
        {
             SqlCommand command = new SqlCommand(NameProcedure, CadenaConexion);
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.Parameters.Add(new SqlParameter("@IdMeSaje", IdKaizalaMsj));
            command.ExecuteNonQuery();
            CadenaConexion.Close();
        }

        public void desconectar()
        {
            try
            {
                CadenaConexion.Close();
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
