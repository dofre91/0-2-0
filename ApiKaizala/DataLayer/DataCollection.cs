﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiKaizala.DataLayer
{
    class DataCollection
    {

        private string NOR_GetTextMachineDelay = "NOR_GetTextMachineDelay"; // fieldNOR_SetTextMachineDelay
        private string NOR_SetTextMachineDelay = "NOR_SetTextMachineDelay";
        private string NOR_SetDummyMachineDelay = "NOR_SetDummyMachineDelay";
        private string NOR_GetDummyMachineDelay = "NOR_GetDummyMachineDelay";
        private string MX_Kzl_GetVarAlerts = "MX_Kzl_GetVarAlerts";
        private string MX_Kzl_SetVarAlerts = "MX_Kzl_SetVarAlerts";



        public string mX_Kzl_GetVarAlerts  // property
        {
            get { return MX_Kzl_GetVarAlerts; }   // get method
            set { MX_Kzl_GetVarAlerts = value; }  // set method
        }

        public string mX_Kzl_SetVarAlerts  // property
        {
            get { return MX_Kzl_SetVarAlerts; }   // get method
            set { MX_Kzl_SetVarAlerts = value; }  // set method
        }

        public string nOR_GetTextMachineDelay   // property
        {
            get { return NOR_GetTextMachineDelay; }   // get method
            set { NOR_GetTextMachineDelay = value; }  // set method
        }

        public string nOR_SetDummyMachineDelay   // property
        {
            get { return NOR_SetDummyMachineDelay; }   // get method
            set { NOR_SetDummyMachineDelay = value; }  // set method
        }

        public string nOR_GetDummyMachineDelay   // property
        {
            get { return NOR_GetDummyMachineDelay; }   // get method
            set { NOR_GetDummyMachineDelay = value; }  // set method
        }

        public string nOR_SetTextMachineDelay   // property
        {
            get { return NOR_SetTextMachineDelay; }   // get method
            set { NOR_SetTextMachineDelay = value; }  // set method
        }


    }
}
